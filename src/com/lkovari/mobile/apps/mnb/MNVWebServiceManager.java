package com.lkovari.mobile.apps.mnb;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

/**
 * http://www.androidpeople.com/android-xml-parsing-tutorial-%E2%80%93-using-domparser
 * @author lkovari
 *
 */
public class MNVWebServiceManager {
	private static final String SOAP_ACTION = "http://www.mnb.hu/webservices/GetCurrentExchangeRates";
	private static final String NAMESPACE = "http://www.mnb.hu";
	private static final String URL = "http://www.mnb.hu/arfolyamok.asmx?WSDL";
	
	
	
	public static String getCurrentExchangeRates() throws IOException, XmlPullParserException {
		SoapPrimitive request = new SoapPrimitive(NAMESPACE, "GetCurrentExchangeRates", ""); //set up request
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); //put all required data into a soap envelope
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request); //prepare request
		HttpTransportSE httpTransport = new HttpTransportSE(URL);
		httpTransport.debug = true; //this is optional, use it if you don't want to use a packet sniffer to check what the sent message was (httpTransport.requestDump)
		httpTransport.call(SOAP_ACTION, envelope); //send request
		SoapPrimitive result = (SoapPrimitive)envelope.getResponse(); //get response
		return result.toString();
	}
}
