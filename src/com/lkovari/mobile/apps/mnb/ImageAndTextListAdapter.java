package com.lkovari.mobile.apps.mnb;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAndTextListAdapter extends ArrayAdapter<ImageAndText> {
	 
    public ImageAndTextListAdapter(Activity activity, List<ImageAndText> imageAndTexts) {
        super(activity, 0, imageAndTexts);
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Activity activity = (Activity) getContext();
        LayoutInflater inflater = activity.getLayoutInflater();
 
        // Inflate the views from XML
        View rowView = inflater.inflate(R.layout.imageandtextlistitem, null);
        ImageAndText imageAndText = getItem(position);
 
        // Load the image and set it on the ImageView
        ImageView imageView = (ImageView) rowView.findViewById(R.id.country_icon);
        imageView.setImageResource(imageAndText.getImageId());
 
        // Set the text on the TextView
        TextView currencyName = (TextView) rowView.findViewById(R.id.currency_name);
        currencyName.setText(imageAndText.getCurrencyName());

        TextView currencyRate = (TextView) rowView.findViewById(R.id.currency_rate);
        currencyRate.setText(String.format("%.02f", imageAndText.getCurrencyRate()));
        
        return rowView;
    }
 
}
