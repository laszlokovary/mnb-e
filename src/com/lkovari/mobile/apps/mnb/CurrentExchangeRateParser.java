package com.lkovari.mobile.apps.mnb;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * 
 * @author lkovari
 *
 */
public class CurrentExchangeRateParser {
	private String exchangeRates;
	private List<ExchangeRate> exchangeRateList = null;
	private Date date = null;
	
	public CurrentExchangeRateParser(String rates) {
		this.exchangeRates = rates;
		exchangeRateList = null;
	}
	
	@Override
	protected void finalize() throws Throwable {
		exchangeRates = null;
		if (exchangeRateList == null) {
			exchangeRateList.clear();
			exchangeRateList = null;
		}	
		super.finalize();
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	private void parseExchangeRates() throws Exception {
		InputStream inputStream = new ByteArrayInputStream(exchangeRates.getBytes("UTF-8"));
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder;
		documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document;
		document = documentBuilder.parse(new InputSource(inputStream));
		document.getDocumentElement().normalize();
		NodeList nodeList = document.getElementsByTagName("Day");	
		for (int ix = 0; ix < nodeList.getLength(); ix++) {
			Node node = nodeList.item(ix);
			if (node != null) {
				NodeList childNodeList = node.getChildNodes();
				for (int iix = 0; iix < childNodeList.getLength(); iix++) {
					Node rateNode = childNodeList.item(iix);
					if (rateNode != null) {
						NamedNodeMap attributes = rateNode.getAttributes();
						Node currNode = attributes.getNamedItem("curr");
						String currency = currNode.getNodeValue();
						Node unitNode = attributes.getNamedItem("unit");
						String unit = unitNode.getNodeValue();
						Node nodeFirst = rateNode.getFirstChild();
						String rate = nodeFirst.getNodeValue();
						ExchangeRate exchangeRate = new ExchangeRate(currency, rate, new Integer(unit));
						exchangeRateList.add(exchangeRate);
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Date getDate() throws Exception {
		if (exchangeRateList == null) {
			exchangeRateList = new ArrayList<ExchangeRate>();
			parseExchangeRates();
		}
		return date;
	}
	
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<ExchangeRate> getExchangeRates() throws Exception {
		if (exchangeRateList == null) {
			exchangeRateList = new ArrayList<ExchangeRate>();
			parseExchangeRates();
		}
		return exchangeRateList;
	}
	
}
