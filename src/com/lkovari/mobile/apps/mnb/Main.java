package com.lkovari.mobile.apps.mnb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 
 * @author lkovari
 *
 */
public class Main extends Activity {
	private ListView listView;
	protected Map<String, Integer> country2Image = new HashMap<String, Integer>();
	protected Map<String, Integer> country2String = new HashMap<String, Integer>();
	protected Map<String, String> currency2Rate = new HashMap<String, String>();
	
	
	private void initializeCurrencies() {
		country2Image.put("AUD", R.drawable.f_aud);
		country2Image.put("BGN", R.drawable.f_bgn);
		country2Image.put("BRL", R.drawable.f_brl);
		country2Image.put("CAD", R.drawable.f_cad);
		country2Image.put("CHF", R.drawable.f_chf);
		country2Image.put("CNY", R.drawable.f_cny);
		country2Image.put("CZK", R.drawable.f_czk);
		country2Image.put("DKK", R.drawable.f_dkk);
		country2Image.put("EEK", R.drawable.f_eek);
		country2Image.put("EUR", R.drawable.f_eur);
		country2Image.put("GBP", R.drawable.f_gbp);
		country2Image.put("HKD", R.drawable.f_hkd);
		country2Image.put("ISK", R.drawable.f_isk);
		country2Image.put("JPY", R.drawable.f_jpy);
		country2Image.put("KRW", R.drawable.f_krw);
		country2Image.put("LTL", R.drawable.f_ltl);
		country2Image.put("LVL", R.drawable.f_lvl);
		country2Image.put("MNX", R.drawable.f_mnx);
		country2Image.put("NOK", R.drawable.f_nok);
		country2Image.put("NZD", R.drawable.f_nzd);
		country2Image.put("PLN", R.drawable.f_pln);
		country2Image.put("RON", R.drawable.f_ron);
		country2Image.put("RSD", R.drawable.f_rsd);
		country2Image.put("RUB", R.drawable.f_rub);
		country2Image.put("SEK", R.drawable.f_sek);
		country2Image.put("SGD", R.drawable.f_sgd);
		country2Image.put("TRY", R.drawable.f_try);
		country2Image.put("UAH", R.drawable.f_uah);
		country2Image.put("USD", R.drawable.f_usd);
		country2Image.put("ZAR", R.drawable.f_zar);

		country2String.put("AUD", R.string.M_AUD);
		country2String.put("BGN", R.string.M_BGN);
		country2String.put("BRL", R.string.M_BRL);
		country2String.put("CAD", R.string.M_CAD);
		country2String.put("CHF", R.string.M_CHF);
		country2String.put("CNY", R.string.M_CNY);
		country2String.put("CZK", R.string.M_CZK);
		country2String.put("DKK", R.string.M_DKK);
		country2String.put("EEK", R.string.M_EEK);
		country2String.put("EUR", R.string.M_EUR);
		country2String.put("GBP", R.string.M_GBP);
		country2String.put("HKD", R.string.M_HKD);
		country2String.put("ISK", R.string.M_ISK);
		country2String.put("JPY", R.string.M_JPY);
		country2String.put("KRW", R.string.M_KRW);
		country2String.put("LTL", R.string.M_LTL);
		country2String.put("LVL", R.string.M_LVL);
		country2String.put("MNX", R.string.M_MNX);
		country2String.put("NOK", R.string.M_NOK);
		country2String.put("NZD", R.string.M_NZD);
		country2String.put("PLN", R.string.M_PLN);
		country2String.put("RON", R.string.M_RON);
		country2String.put("RSD", R.string.M_RSD);
		country2String.put("RUB", R.string.M_RUB);
		country2String.put("SEK", R.string.M_SEK);
		country2String.put("SGD", R.string.M_SGD);
		country2String.put("TRY", R.string.M_TRY);
		country2String.put("UAH", R.string.M_UAH);
		country2String.put("USD", R.string.M_USD);
		country2String.put("ZAR", R.string.M_ZAR);
	}
	
	
	private void checkMobileOrWiFiServices(boolean wasException) {
		String mess = "";
		boolean isShowMessage = false;
		if (wasException) {
			Resources resources = getResources();
			// isMobile connected?
			if (!ConnectionManager.isMobileConnected(this)) { 
				mess =  resources.getString(R.string.error_nomobiledataconnection) + "\n";
				isShowMessage = true;
			}	
			if (!ConnectionManager.isWiFiConnected(this)) { 
				mess += resources.getString(R.string.error_nowificonnection);
				isShowMessage = true;
			}	
			if (isShowMessage) {
				Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();
			}
		}
		else {
			// isMobile connected?
//FIXME how to check?			
		}
	}
	
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.main);
		//initialize items
		initializeCurrencies();
		List<ImageAndText> imagesAndTexts = new ArrayList<ImageAndText>();
		
		//get MNB. exchange rates
		String rates = null;
		try {
			rates = MNVWebServiceManager.getCurrentExchangeRates();
		} catch (IOException e) {
			e.printStackTrace();
//			Toast.makeText(getApplicationContext(), R.string.error_ioexception, Toast.LENGTH_LONG).show();
			checkMobileOrWiFiServices(true);
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), R.string.error_xmlpullparserexception, Toast.LENGTH_LONG).show();
		}
		CurrentExchangeRateParser currentExchangeRateParser = new CurrentExchangeRateParser(rates);
		List<ExchangeRate> exchangeRateList = null;
		try {
			exchangeRateList = currentExchangeRateParser.getExchangeRates();
			currentExchangeRateParser = null;
			for (ExchangeRate er : exchangeRateList) {
				System.out.println(er.getCurrency() + " " + er.getRate());
				if (country2Image.containsKey(er.getCurrency())) {
					int imageId = country2Image.get(er.getCurrency());
					String currencyLongName = this.getResources().getString(country2String.get(er.getCurrency()));
					String currencyShortName = er.getCurrency();
					double currencyRate = er.getRate();
					if (er.getUnit() != 1) {
						currencyLongName += " (" +er.getUnit() + ")";
					}
					ImageAndText imageListItem = new ImageAndText(imageId, currencyShortName + " " + currencyLongName, currencyRate);
					imagesAndTexts.add(imageListItem);
				}
				else
					Log.d("", "Currency Name " + er.getCurrency() + " not found in country2Image container!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), R.string.error_ratesprocessingexception, Toast.LENGTH_LONG).show();
		}
		
		ImageAndTextListAdapter imageAndTextListAdapter = new ImageAndTextListAdapter(this, imagesAndTexts); 
		listView=(ListView)findViewById(R.id.currenciesListView);
		// By using setAdpater method in listview we an add string array in list.
//		listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1 , menuItems));
		listView.setAdapter(imageAndTextListAdapter);
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.optionsmenu, menu);
		return true;
	}

	/* Handles item selections */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.about:
		        Intent intent = new Intent();
		        intent.setClass(getApplicationContext(), Info.class);
		        startActivity(intent);      
				return true;
			case R.id.exit:
		        Intent intentData = new Intent();
		        intentData.putExtra("IS_EXIT", new Boolean(true));
		        setResult(android.app.Activity.RESULT_OK, intentData);				
				finish();

				System.exit(1);
//				moveTaskToBack(true);
//				int pid = android.os.Process.myPid();
//				android.os.Process.killProcess(pid); 
				return true;
		}
		return false;
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onDestroy() {
		country2Image.clear();
		country2String.clear();
		currency2Rate.clear();
		country2Image = null;
		country2String = null;
		currency2Rate = null;
		super.onDestroy();
	}
	
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
    }    
	
}
